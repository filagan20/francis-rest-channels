from django.conf.urls import url

from . import consumers

websocket_urlpatterns = [
    url(r'ws/schoolapp/course/$', consumers.CourseConsumer),
    url(r'ws/schoolapp/subject/$', consumers.SubjectConsumer),
    url(r'ws/schoolapp/teacher/$', consumers.TeacherConsumer),
    url(r'ws/schoolapp/class/$', consumers.ClassConsumer),
    url(r'ws/schoolapp/student/$', consumers.StudentConsumer),
]

from django.core.exceptions import ValidationError

from django.db import models

# Create your models here.


class Course(models.Model):
    name = models.TextField(max_length=100)
    code = models.TextField(max_length=100)


class Subject(models.Model):
    name = models.TextField(max_length=100)
    code = models.TextField(max_length=100)
    year_level = models.PositiveSmallIntegerField(blank=True, null=True)
    course = models.ManyToManyField(Course, blank=True)


class Teacher(models.Model):
    first_name = models.TextField(max_length=100)
    last_name = models.TextField(max_length=100)
    subjects = models.ManyToManyField(Subject, blank=True)


class Student(models.Model):
    first_name = models.TextField(max_length=100)
    last_name = models.TextField(max_length=100)
    year_level = models.PositiveSmallIntegerField()
    course = models.ForeignKey(
        Course, on_delete=models.SET_NULL, null=True, blank=True)


class Class(models.Model):
    name = models.TextField(max_length=100)
    max_capacity = models.PositiveIntegerField()
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    teacher = models.ForeignKey(
        Teacher, on_delete=models.SET_NULL, null=True, blank=True)
    students = models.ManyToManyField(Student, blank=True)

    def clean_students(self, *args, **kwargs):
        # Students
        student_list = self.students.all()
        if len(student_list) > self.max_capacity:
            raise ValidationError("Student count is greater than max capacity")

    def save(self, *args, **kwargs):
        self.full_clean()
        super(Class, self).save(*args, **kwargs)

from django.core.exceptions import ObjectDoesNotExist, ValidationError

from .models import Teacher, Subject, Class, Student, Course
from .serializers import (
    TeacherSerializer,
    ClassSerializer,
    SubjectSerializer,
    StudentSerializer,
    CourseSerializer)
import json


class SchoolActions(object):
    model = None
    serializer_class = None

    def run_action(self, request):
        pass


class ObjectList(SchoolActions):
    def __init__(self, model, serializer_class):
        self.model = model
        self.serializer_class = serializer_class

    def run_action(self, request):
        objectlist = self.model.objects.all()
        serializer = self.serializer_class(objectlist, many=True)
        message = {
            'status_code': 200,
            'message': 'List Returned',
            'data': serializer.data
        }
        return message


class ObjectCreate(SchoolActions):
    def __init__(self, model, serializer_class):
        self.model = model
        self.serializer_class = serializer_class

    def run_action(self, request):
        data = request['data']
        serializer = self.serializer_class(data=data)
        if serializer.is_valid():
            serializer.save()
            message = {
                'status_code': 200,
                'message': 'Record Created',
                'data': serializer.data
            }
        else:
            message = {
                'status_code': 400,
                'message': 'Bad Request',
                'errors': serializer.errors
            }
        return message


class ObjectRetrieve(SchoolActions):
    def __init__(self, model, serializer_class):
        self.model = model
        self.serializer_class = serializer_class

    def run_action(self, request):
        req_id = request['id']
        try:
            instance = self.model.objects.get(pk=req_id)
            serializer = self.serializer_class(instance)
            message = {
                'status_code': 200,
                'message': 'Record Returned',
                'data': serializer.data
            }
        except ObjectDoesNotExist:
            message = {
                'status_code': 404,
                'message': 'No Object Found'
            }
        return message


class ObjectUpdate(SchoolActions):
    def __init__(self, model, serializer_class):
        self.model = model
        self.serializer_class = serializer_class

    def run_action(self, request):
        req_id = request['id']
        data = request['data']
        try:
            instance = self.model.objects.get(pk=req_id)
            serializer = self.serializer_class(
                instance, data=data, partial=True)
            if serializer.is_valid():
                serializer.save()
                message = {
                    'status_code': 200,
                    'message': 'Record Updated',
                    'data': serializer.data
                }
            else:
                message = {
                    'status_code': 400,
                    'message': 'Bad Request',
                    'errors': serializer.errors
                }
        except ObjectDoesNotExist:
            message = {
                'status_code': 404,
                'message': 'No Object Found'
            }
        return message


class ObjectDelete(SchoolActions):
    def __init__(self, model, serializer_class):
        self.model = model
        self.serializer_class = serializer_class

    def run_action(self, request):
        req_id = request['id']
        try:
            instance = self.model.objects.get(pk=req_id)
            instance.delete()
            message = {
                'status_code': 200,
                'message': 'Record Deleted',
            }
        except ObjectDoesNotExist:
            message = {
                'status_code': 404,
                'message': 'No Object Found'
            }
        return message


class SubjectGetTeachers(SchoolActions):
    def __init__(self):
        self.model = Teacher
        self.serializer_class = TeacherSerializer

    def run_action(self, request):
        req_id = request['id']
        try:
            teacher_list = self.model.objects.filter(
                subjects__pk__exact=req_id)
            serializer = self.serializer_class(teacher_list, many=True)
            message = {
                'status_code': 200,
                'message': 'List Returned',
                'data': serializer.data
            }
        except ObjectDoesNotExist:
            message = {
                'status_code': 404,
                'message': 'No Object Found'
            }
        return message


class ClassStudent(SchoolActions):
    def __init__(self):
        self.model = Class
        self.serializer_class = ClassSerializer

    def run_action(self, request):
        method = request['method']
        req_id = request['id']
        student_list_pk = request['student_list']
        try:
            class_object = Class.objects.get(pk=req_id)
            year_level = class_object.subject.year_level
            if year_level != None and method == "add":
                student_list = Student.objects.filter(
                    pk__in=student_list_pk, year_level__gte=year_level)
            else:
                student_list = Student.objects.filter(pk__in=student_list_pk)

            success = True

            if method == "add":
                student_list = student_list.exclude(class__pk=req_id)
                new_student_count = len(student_list) + \
                    class_object.students.count()
                if new_student_count > class_object.max_capacity:
                    success = False
                    message = {
                        'status_code': 400,
                        'message': 'Bad Request',
                        'errors': {
                            'students': 'Student count is greater than max capacity'
                        }
                    }
                else:
                    class_object.students.add(*student_list)
            elif method == "remove":
                class_object.students.remove(*student_list)

            if success:
                serializer = self.serializer_class(class_object)
                message = {
                    'status_code': 200,
                    'message': 'Student List Updated',
                    'data': serializer.data
                }
        except ObjectDoesNotExist:
            message = {
                'status_code': 404,
                'message': 'No Object Found'
            }
        return message


class StudentGetClass(SchoolActions):
    def __init__(self):
        self.model = Class
        self.serializer_class = ClassSerializer

    def run_action(self, request):
        req_id = request['id']
        class_list = self.model.objects.filter(
            students__pk__exact=req_id)
        serializer = self.serializer_class(class_list, many=True)
        message = {
            'status_code': 200,
            'message': 'List Returned',
            'data': serializer.data
        }
        return message


class StudentGetRequiredSubjects(SchoolActions):
    def run_action(self, request):
        req_id = request['id']
        try:
            student = Student.objects.get(pk=req_id)
            course = student.course
            subject_list = Subject.objects.filter(course=course)
            serializer = SubjectSerializer(subject_list, many=True)
            message = {
                'status_code': 200,
                'message': 'List Returned',
                'data': serializer.data
            }
        except ObjectDoesNotExist:
            message = {
                'status_code': 404,
                'message': 'No Object Found'
            }
        return message


class StudentGetTeachers(SchoolActions):
    def run_action(self, request):
        req_id = request['id']
        try:
            teacher_list = Teacher.objects.filter(
                class__students__pk__exact=req_id)
            serializer = TeacherSerializer(teacher_list, many=True)
            message = {
                'status_code': 200,
                'message': 'List Returned',
                'data': serializer.data
            }
        except ObjectDoesNotExist:
            message = {
                'status_code': 404,
                'message': 'No Object Found'
            }
        return message


class ClassGetStudents(SchoolActions):
    def run_action(self, request):
        req_id = request['id']
        try:
            class_object = Class.objects.get(pk=req_id)
            student_list = class_object.students.all()
            serializer = StudentSerializer(student_list, many=True)
            message = {
                'status_code': 200,
                'message': 'List Returned',
                'data': serializer.data
            }
        except ObjectDoesNotExist:
            message = {
                'status_code': 404,
                'message': 'No Object Found'
            }
        return message


class SubjectGetCourses(SchoolActions):
    def run_action(self, request):
        req_id = request['id']
        try:
            subject = Subject.objects.get(pk=req_id)
            course_list = subject.course.all()
            serializer = CourseSerializer(course_list, many=True)
            message = {
                'status_code': 200,
                'message': 'List Returned',
                'data': serializer.data
            }
        except ObjectDoesNotExist:
            message = {
                'status_code': 404,
                'message': 'No Object Found'
            }
        return message


class ClassGetCount(SchoolActions):
    def run_action(self, request):
        classes = Class.objects.all()
        serializer = ClassSerializer(classes, many=True)
        message = {
            'status_code': 200,
            'message': 'List Returned',
            'data': serializer.data
        }
        return message


class CourseGetCount(SchoolActions):
    def run_action(self, request):
        courses = Course.objects.all()
        serializer = CourseSerializer(courses, many=True)
        message = {
            'status_code': 200,
            'message': 'List Returned',
            'data': serializer.data
        }
        return message

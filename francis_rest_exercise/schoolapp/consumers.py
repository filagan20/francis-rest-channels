from channels.generic.websocket import AsyncWebsocketConsumer
from .models import Course, Subject, Teacher, Class, Student
from .serializers import (
    CourseSerializer,
    SubjectSerializer,
    TeacherSerializer,
    ClassSerializer,
    StudentSerializer,
)
from . import api_actions

import json


class ObjectConsumer(AsyncWebsocketConsumer):
    model = None
    serializer_class = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.actions = {
            "list": api_actions.ObjectList(self.model, self.serializer_class),
            "view": api_actions.ObjectRetrieve(self.model, self.serializer_class),
            "create": api_actions.ObjectCreate(self.model, self.serializer_class),
            "update": api_actions.ObjectUpdate(self.model, self.serializer_class),
            "delete": api_actions.ObjectDelete(self.model, self.serializer_class),
        }

    async def connect(self):
        await self.accept()

    async def disconnect(self, close_code):
        pass

    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        action = text_data_json['action']

        resp_message = {}

        if action in self.actions.keys():
            resp_message = self.actions[action].run_action(
                request=text_data_json)
        else:
            resp_message = {
                'status_code': 400,
                'message': 'Bad Request',
            }

        await self.send(text_data=json.dumps(resp_message))


class CourseConsumer(ObjectConsumer):
    model = Course
    serializer_class = CourseSerializer
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.actions["get_count"] = api_actions.CourseGetCount()

class SubjectConsumer(ObjectConsumer):
    model = Subject
    serializer_class = SubjectSerializer

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.actions["get_teachers"] = api_actions.SubjectGetTeachers()
        self.actions["get_courses"] = api_actions.SubjectGetCourses()


class TeacherConsumer(ObjectConsumer):
    model = Teacher
    serializer_class = TeacherSerializer


class ClassConsumer(ObjectConsumer):
    model = Class
    serializer_class = ClassSerializer

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.actions["add_delete_student"] = api_actions.ClassStudent()
        self.actions["get_students"] = api_actions.ClassGetStudents()
        self.actions["get_count"] = api_actions.ClassGetCount()

class StudentConsumer(ObjectConsumer):
    model = Student
    serializer_class = StudentSerializer

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.actions["get_teachers"] = api_actions.StudentGetTeachers()
        self.actions["get_required_subjects"] = api_actions.StudentGetRequiredSubjects()
        self.actions["get_classes"] = api_actions.StudentGetClass()

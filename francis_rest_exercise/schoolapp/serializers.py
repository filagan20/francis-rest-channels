from rest_framework import serializers
from .models import Course, Subject, Teacher, Class, Student


class CourseSerializer(serializers.ModelSerializer):
    student_count = serializers.SerializerMethodField()
    teacher_count = serializers.SerializerMethodField()
    class Meta:
        model = Course
        fields = ('id', 'name', 'code', 'student_count', 'teacher_count')

    def get_student_count(self, obj):
        return obj.student_set.count()
    def get_teacher_count(self,obj):
        return Teacher.objects.filter(subjects__course__pk = obj.pk).count()

class SubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subject
        fields = ('id', 'name', 'code', 'year_level', 'course')


class TeacherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Teacher
        fields = ('id', 'first_name', 'last_name', 'subjects')


class ClassSerializer(serializers.ModelSerializer):
    student_count = serializers.SerializerMethodField()
    class Meta:
        model = Class
        fields = ('id', 'name', 'max_capacity',
                  'subject', 'teacher', 'students', 'student_count')

    def get_student_count(self, obj):
        return obj.students.count()

    def validate_students(self, value):
        """
        Check if student is of max capacity
        """
        initial_data = self.instance
        year_level = initial_data.subject.year_level
        if len(value) > initial_data.max_capacity:
            raise serializers.ValidationError(
                "Student count is greater than max capacity.")
        if year_level != None:
            for student in value:
                student_year = student.year_level
                if year_level != student_year:
                    raise serializers.ValidationError(
                        "There are unallowed students in the list")
        return value

    def validate_teacher(self, value):
        """
        Check if teacher has right to study the subject
        """
        if value != None:
            initial_data = self.instance
            if initial_data == None:
                initial_data = self.initial_data
                instance_subject = Subject.objects.get(
                    pk=initial_data['subject'])
            else:
                instance_subject = initial_data.subject
            tSerializer = TeacherSerializer(value)
            tData = tSerializer.data
            tSubjects = tData['subjects']
            if instance_subject.pk not in tSubjects:
                raise serializers.ValidationError(
                    "Teacher can't teach the subject given")
        return value


class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ('id', 'first_name', 'last_name',
                  'year_level', 'course')
